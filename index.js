const ipinfo = require("ipinfo")
const ips = require("./ips")
const token = require("./token")
const fs = require("fs")

function getIpInfo(ip) {
    ipinfo(ip, token, (err, data) => {
        console.log(err || data)
        fs.appendFile("log", `${JSON.stringify(data)},\n`, (err) => {
            console.error(err)
        })
    })
}

ips.forEach(ip => {
    getIpInfo(ip)
});